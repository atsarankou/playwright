import { test as setup } from '../support/custom-fixtures';
import { storageState } from '../playwright.config';

setup('Login', async ({ page, loginPage }) => { 
  await page.goto('/login');
  await loginPage.logIn(process.env.EMAIL, process.env.PASSWORD);
  await page.context().storageState({ path: storageState });
});